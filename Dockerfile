FROM python:3.8 AS runtime

# requirements.txt is copied first to make use of Docker caching
COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

ADD audit /app/audit
ADD app-template.yml /app/app-template.yml

WORKDIR /app

CMD gunicorn --bind=0.0.0.0:80 audit.run:application

EXPOSE 80

FROM runtime AS unittest

RUN pip install nose2 coverage wait-for-cassandra


ADD tests /app/tests
ADD tests/test.sh /app/test.sh
RUN chmod ugo+x /app/test.sh

CMD /app/test.sh
