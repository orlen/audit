import os
import unittest

from audit import __version__
from audit.run import application


class TestApplication(unittest.TestCase):
    def setUp(self):
        self.client = application.test_client()

    def test_version(self):
        resp = self.client.get('/v1/version')
        self.assertEquals(resp.json, {'version': __version__})

    def test_add_log_and_view(self):
        resp = self.client.put('/v1/log', json={
            'what': 'login',
            'who': 'user@example.com',
            'timestamp': 200
        })
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get('/v1/log/0-300?value=user%40example.com')
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp.json, [
            {'timestamp': 200, 'who': 'user@example.com', 'what': 'login'}
        ])

    def test_index(self):
        os.environ['LOGGED_FIELDS'] = 'index'
        resp = self.client.put('/v1/log', json={
            'what': 'login',
            'who': 'user@example.com',
            'timestamp': 2000,
            'metadata': {
                'index': '1'
            }
        })
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get('/v1/log/0-3000?value=1&field_name=index')
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp.json, [
            {'timestamp': 2000, 'who': 'user@example.com', 'what': 'login',
             'metadata': {'index': '1'}}
        ])
