# Audit microservice for Endpoints application
[![pipeline status](https://git.cervirobotics.com/dronhub/orlen-backend/audit/badges/develop/pipeline.svg)](https://git.cervirobotics.com/dronhub/orlen-backend/audit/commits/develop)

# Environment variables
* **CASSANDRA_HOST** - host on which Cassandra runs. Defaults to *cassandra*
* **CASSANDRA_KEYSPACE** - keyspace to use on target Cassandra. Defaults to *api*
* **CASSANDRA_USERNAME** - username to use for authentication with Cassandra. Default is *cassandra*
* **CASSANDRA_PASSWORD** - password to use for authentication with Cassandra. Default is *cassandra*
* **CASSANDRA_DC** - name of the local DC. By default *datacenter1*, as such is the default in Cassandra
* **LOGGED_FIELDS** - space separated fields of metadata that will be indexed

# Required schema installed in Cassandra

Refer [here](tests/dockerfiles/cassandra/schema.cql) to learn more about
schema required for Audit to work.

# Development
Use docker-compose.yml service unittest to launch unit tests. This has
zero requirements and will build everything from scratch.

# Endpoints
All endpoints are documented [here](audit/handlers.py).
