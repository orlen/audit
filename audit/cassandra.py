import os

import cassandra.cluster
import cassandra.auth
from cassandra.query import BatchStatement, SimpleStatement


class BatchStatementSessionWrapper:
    def __init__(self, cass):
        self.cass = cass
        self.bs: BatchStatement = None

    def __enter__(self):
        self.bs = BatchStatement()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            self.cass.session.execute(self.bs)
            self.bs = None

    def execute(self, query, args):
        stmt = SimpleStatement(query)
        self.bs.add(stmt, args)


class CassandraCluster:
    def __init__(self):
        self.cluster = cassandra.cluster.Cluster([os.environ.get('CASSANDRA_HOST', 'cassandra')],
                                                 load_balancing_policy=cassandra.cluster.TokenAwarePolicy(
                                                     cassandra.cluster.DCAwareRoundRobinPolicy(
                                                         local_dc=os.environ.get('CASSANDRA_DC', 'datacenter1')
                                                     )
                                                 ),
                                                 auth_provider=cassandra.auth.PlainTextAuthProvider(
                                                     username=os.environ.get('CASSANDRA_USERNAME', 'cassandra'),
                                                     password=os.environ.get('CASSANDRA_PASSWORD', 'cassandra')
                                                 ))
        self.session = self.cluster.connect(os.environ.get('CASSANDRA_KEYSPACE', 'audit'))

    def batch_query(self):
        return BatchStatementSessionWrapper(self)

    def __enter__(self):
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        return False


Cassandra: CassandraCluster = CassandraCluster()
