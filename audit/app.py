import yaml
from flask import Flask
from flask_json import FlaskJSON
from flasgger import Swagger
from . import __version__

app = Flask(__name__)
app.config['JSON_ADD_STATUS'] = False
json_app = FlaskJSON(app)
json_app.init_app(app)

with open('/app/app-template.yml', 'r') as f_in:
    template = yaml.load(f_in, Loader=yaml.Loader)

template['info']['version'] = __version__
app.config['SWAGGER'] = {'openapi': '3.0.2'}
Swagger(app, template=template)


