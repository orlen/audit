import typing as tp
import os

import ujson
from flask import request
from flask_json import as_json
from satella.coding import silence_excs
from satella.time import time_as_int

from . import __version__
from .app import app
from .cassandra import Cassandra


def get_logged_fields() -> tp.Set[str]:
    return set(os.environ.get('LOGGED_FIELDS', '').split(' '))


@app.route("/v1/version", methods=['GET'])
@as_json
def version() -> dict:
    """
    Return current version of the Audit microservice
    ---
    responses:
        '200':
            description: Here's your response
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            version:
                                type: string
                                description: Version of the microservice
    """
    return {'version': __version__}


@app.route("/v1/log", methods=['PUT'])
@as_json
def log() -> dict:
    """
    Log an entry
    ---
    requestBody:
        description: Data to log
        content:
            application/json:
                schema:
                    type: object
                    properties:
                        timestamp:
                            type: integer
                            description: >
                                UNIX timestamp of the event. Current one will be put
                                if not given.
                        who:
                            type: string
                            description: Identifier of the entity effecting the change
                        what:
                            type: string
                            description: Machine-readable event identifier
                        metadata:
                            type: object
                            additionalProperties: true
                    required:
                        - who
                        - what
    responses:
        '200':
            description: Stored OK
        '400':
            description: You have an error in one of fields
    """
    data: dict = request.get_json(force=True)
    if 'timestamp' not in data:
        data['timestamp'] = time_as_int()
    if 'metadata' not in data:
        data['metadata'] = {}

    args = (data['who'], data['timestamp'], ujson.dumps(data['metadata']), data['what'])

    with Cassandra.batch_query() as cur:
        cur.execute('INSERT INTO audit_log (who, timestamp, metadata, what) '
                    'VALUES (%s, %s, %s, %s)', args)

        for key in get_logged_fields():
            if key in data['metadata']:
                cur.execute('INSERT INTO audit_log_index (field_name, field_value, '
                            'who, timestamp, metadata, what) VALUES (%s, %s, %s, %s, %s, %s)',
                            (key, data['metadata'][key]) + args)

    return {}


@app.route("/v1/log/<int:from_>-<int:to>", methods=['GET'])
@as_json
def get(from_: int, to: int) -> dict:
    """
    Return all audit entries for given period.

    This has two modes. It can either query the issuer database (don't provide field_name0
    or it can query an index (in this case you provide a field_name).
    ---
    parameters:
       - in: query
         name: value
         required: true
         schema:
            type: string
         description: Name of the event issuer OR field value, if field name is ivem
       - in: path
         name: from_
         schema:
           type: integer
         description: UNIX timestamp of the beginning frm the time period
         required: true
       - in: path
         required: true
         name: to
         schema:
           type: integer
         description: UNIX timestamp of the end of the time period
       - in: query
         required: false
         name: field_name
         description: Optional field name to query the index instead
         schema:
            type: string
    responses:
        '200':
            description: OK, here's your list
            content:
                application/json:
                    schema:
                        type: array
                        items:
                            $ref: '#/components/schemas/LogEvent'
        '400':
            description: Malformed request
    """
    query_index = 'field_name' in request.args

    if query_index:
        que = 'SELECT who, timestamp, metadata, what FROM audit_log_index WHERE ' \
              'field_name=%s AND field_value=%s AND timestamp >= %s AND timestamp <= %s'
        args = (request.args['field_name'], request.args['value'], from_, to)
    else:
        que = 'SELECT who, timestamp, metadata, what FROM audit_log WHERE who=%s ' \
              'AND timestamp >= %s AND timestamp <= %s'
        args = (request.args['value'], from_, to)

    with Cassandra as cur:
        p = cur.execute(que, args)

    output = []
    for who, timestamp, metadata, what in p:

        result = {'timestamp': timestamp, 'who': who, 'what': what}
        with silence_excs(ValueError):
            metadata = ujson.loads(metadata)
            if metadata:
                result['metadata'] = metadata

        output.append(result)
    return output
