from flask import Response

from .app import app


@app.errorhandler(KeyError)
@app.errorhandler(TypeError)
@app.errorhandler(ValueError)
def handle_error(e):
    return Response(repr(e), status=400)

